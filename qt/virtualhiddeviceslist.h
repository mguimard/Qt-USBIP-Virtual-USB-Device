#ifndef VIRTUALHIDDEVICESLIST_H
#define VIRTUALHIDDEVICESLIST_H
#include "virtualhiddeviceview.h"

#include <QWidget>

namespace Ui {
class VirtualHidDevicesList;
}

class VirtualHidDevicesList : public QWidget
{
    Q_OBJECT

public:
    explicit VirtualHidDevicesList(QWidget *parent = nullptr);
    ~VirtualHidDevicesList();

public slots:
    void on_new_device_clicked();

private:
    Ui::VirtualHidDevicesList *ui;    
    std::vector<VirtualHidDeviceView*> views;

    void remove_view(VirtualHidDeviceView*);
    void update_ui();
};

#endif // VIRTUALHIDDEVICESLIST_H
