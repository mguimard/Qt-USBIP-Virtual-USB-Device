#include "virtualhiddeviceview.h"
#include "ui_virtualhiddeviceview.h"

#include <QProcess>

VirtualHidDeviceView::VirtualHidDeviceView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VirtualHidDeviceView)
{
    ui->setupUi(this);

    device = new USBIPDevice();

    // Cougar revenger ST
    //ui->vendor_id->setText("12CF");
    //ui->product_id->setText("0412");
    //ui->I->setText("0");
    //ui->P->setText("1");
    //ui->U->setText("2");

    // MSI OPTIX
    ui->vendor_id->setText("1462");
    ui->product_id->setText("3FA4");
    ui->I->setText("0");
    ui->P->setText("FF00");
    ui->U->setText("1");

    device->OnStateChanged([=](){
        UpdateState(device->IsRunning());
    });

    device->OnMessage([=](std::string message){
        AddMessage(message);
    });

    device->OnAttachReady([=](){
        AddMessage("Run from a terminal:");
        AddMessage("sudo usbip --tcp-port " +
                   std::to_string(device->GetServerPort()) +
                   " attach -r 127.0.0.1 -b 1-1");
    });

    device->OnAttached([=](){

    });

    UpdateState(false);
}

void VirtualHidDeviceView::AddMessage(std::string message)
{
    const QString log = QString::fromStdString(message);
    ui->logs->addItem(log);
}

VirtualHidDeviceView::~VirtualHidDeviceView()
{
    delete ui;
}

void VirtualHidDeviceView::on_remove_device_clicked()
{
    device->Stop();
    emit OnRemove();
}

void VirtualHidDeviceView::on_start_clicked()
{
    char* p;

    int vendor_id = strtoul(ui->vendor_id->text().toStdString().c_str(), &p, 16 );

    if (*p != 0)
    {
        AddMessage("Vendor id isnt a correct number");
        return;
    }

    int product_id = strtoul(ui->product_id->text().toStdString().c_str(), &p, 16 );

    if (*p != 0)
    {
        AddMessage("Product id isnt a correct number");
        return;
    }

    unsigned char I = strtoul(ui->I->text().toStdString().c_str(), &p, 16 );

    if (*p != 0)
    {
        AddMessage("Interface isnt a correct number");
        return;
    }

    unsigned int P = strtoul(ui->P->text().toStdString().c_str(), &p, 16 );

    if (*p != 0)
    {
        AddMessage("Page isnt a correct number");
        return;
    }

    unsigned char U = strtoul(ui->U->text().toStdString().c_str(), &p, 16 );

    if (*p != 0)
    {
        AddMessage("Usage isnt a correct number");
        return;
    }

    AddMessage("Device " + ui->vendor_id->text().toStdString() + ":" + ui->product_id->text().toStdString());

    device->SetServerPort(ui->tcp_port->value());
    device->SetIds({vendor_id, product_id, I, P, U});

    device->Start();
}

void VirtualHidDeviceView::on_stop_clicked()
{
    device->Stop();
}

void VirtualHidDeviceView::on_clear_log_clicked()
{
    ui->logs->clear();
}

void VirtualHidDeviceView::UpdateState(bool running)
{
    ui->start->setDisabled(running);
    ui->stop->setDisabled(!running);
}
