#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include "virtualhiddeviceslist.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->centralwidget->setLayout(new QVBoxLayout);

    ui->centralwidget->layout()->addWidget(new VirtualHidDevicesList);
}

MainWindow::~MainWindow()
{
    delete ui;
}

