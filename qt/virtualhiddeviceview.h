#ifndef VIRTUALHIDDEVICEVIEW_H
#define VIRTUALHIDDEVICEVIEW_H

#include "usbip-device.h"

#include <QWidget>

namespace Ui {
class VirtualHidDeviceView;
}

class VirtualHidDeviceView : public QWidget
{
    Q_OBJECT

public:
    explicit VirtualHidDeviceView(QWidget *parent = nullptr);
    ~VirtualHidDeviceView();

public slots:
    void on_remove_device_clicked();
    void on_start_clicked();
    void on_stop_clicked();
    void on_clear_log_clicked();

signals:
    void OnRemove();

private:
    Ui::VirtualHidDeviceView *ui;
    USBIPDevice* device;

    void AddMessage(std::string);
    void UpdateState(bool);
};

#endif // VIRTUALHIDDEVICEVIEW_H
