#include "virtualhiddeviceslist.h"
#include "ui_virtualhiddeviceslist.h"
#include <QVBoxLayout>

VirtualHidDevicesList::VirtualHidDevicesList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VirtualHidDevicesList)
{
    ui->setupUi(this);
    ui->scrollAreaWidgetContents->setLayout(new QVBoxLayout);

    update_ui();
}

VirtualHidDevicesList::~VirtualHidDevicesList()
{
    delete ui;
}

void VirtualHidDevicesList::on_new_device_clicked()
{
    VirtualHidDeviceView* view = new VirtualHidDeviceView(this);
    views.push_back(view);

    connect(view, &VirtualHidDeviceView::OnRemove, [=](){
        ui->scrollAreaWidgetContents->layout()->removeWidget(view);
        views.erase(std::find(views.begin(), views.end(), view));
        delete view;
        update_ui();
    });

    ui->scrollAreaWidgetContents->layout()->addWidget(view);
    update_ui();
}


void VirtualHidDevicesList::update_ui()
{
    bool isEmpty = views.empty();

    ui->instructions_label->setVisible(isEmpty);
    ui->scrollArea->setVisible(!isEmpty);
}
