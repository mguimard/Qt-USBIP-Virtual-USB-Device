#ifndef USBIP_DEVICE_H
#define USBIP_DEVICE_H

#ifdef _WIN32
#include <winsock.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

//system headers independent
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <string>
#include <functional>
#include <thread>

//defines
typedef struct sockaddr sockaddr;

// USB Descriptors
#define USB_DESCRIPTOR_DEVICE           0x01    // Device Descriptor.
#define USB_DESCRIPTOR_CONFIGURATION    0x02    // Configuration Descriptor.
#define USB_DESCRIPTOR_STRING           0x03    // String Descriptor.
#define USB_DESCRIPTOR_INTERFACE        0x04    // Interface Descriptor.
#define USB_DESCRIPTOR_ENDPOINT         0x05    // Endpoint Descriptor.
#define USB_DESCRIPTOR_DEVICE_QUALIFIER 0x06    // Device Qualifier.
#define DESCRIPTOR_SIZE                 5

typedef struct __attribute__ ((__packed__)) _USB_DEVICE_DESCRIPTOR
{
    unsigned char bLength;               // Length of this descriptor.
    unsigned char bDescriptorType;       // DEVICE descriptor type (USB_DESCRIPTOR_DEVICE).
    unsigned short bcdUSB;                // USB Spec Release Number (BCD).
    unsigned char bDeviceClass;          // Class code (assigned by the USB-IF). 0xFF-Vendor specific.
    unsigned char bDeviceSubClass;       // Subclass code (assigned by the USB-IF).
    unsigned char bDeviceProtocol;       // Protocol code (assigned by the USB-IF). 0xFF-Vendor specific.
    unsigned char bMaxPacketSize0;       // Maximum packet size for endpoint 0.
    unsigned short idVendor;              // Vendor ID (assigned by the USB-IF).
    unsigned short idProduct;             // Product ID (assigned by the manufacturer).
    unsigned short bcdDevice;             // Device release number (BCD).
    unsigned char iManufacturer;         // Index of String Descriptor describing the manufacturer.
    unsigned char iProduct;              // Index of String Descriptor describing the product.
    unsigned char iSerialNumber;         // Index of String Descriptor with the device's serial number.
    unsigned char bNumConfigurations;    // Number of possible configurations.
} USB_DEVICE_DESCRIPTOR;

typedef struct __attribute__ ((__packed__)) _USB_CONFIGURATION_DESCRIPTOR
{
    unsigned char bLength;               // Length of this descriptor.
    unsigned char bDescriptorType;       // CONFIGURATION descriptor type (USB_DESCRIPTOR_CONFIGURATION).
    unsigned short wTotalLength;          // Total length of all descriptors for this configuration.
    unsigned char bNumInterfaces;        // Number of interfaces in this configuration.
    unsigned char bConfigurationValue;   // Value of this configuration (1 based).
    unsigned char iConfiguration;        // Index of String Descriptor describing the configuration.
    unsigned char bmAttributes;          // Configuration characteristics.
    unsigned char bMaxPower;             // Maximum power consumed by this configuration.
} USB_CONFIGURATION_DESCRIPTOR;

typedef struct __attribute__ ((__packed__)) _USB_INTERFACE_DESCRIPTOR
{
    unsigned char bLength;               // Length of this descriptor.
    unsigned char bDescriptorType;       // INTERFACE descriptor type (USB_DESCRIPTOR_INTERFACE).
    unsigned char bInterfaceNumber;      // Number of this interface (0 based).
    unsigned char bAlternateSetting;     // Value of this alternate interface setting.
    unsigned char bNumEndpoints;         // Number of endpoints in this interface.
    unsigned char bInterfaceClass;       // Class code (assigned by the USB-IF).  0xFF-Vendor specific.
    unsigned char bInterfaceSubClass;    // Subclass code (assigned by the USB-IF).
    unsigned char bInterfaceProtocol;    // Protocol code (assigned by the USB-IF).  0xFF-Vendor specific.
    unsigned char iInterface;            // Index of String Descriptor describing the interface.
} USB_INTERFACE_DESCRIPTOR;

typedef struct __attribute__ ((__packed__)) _USB_ENDPOINT_DESCRIPTOR
{
    unsigned char bLength;               // Length of this descriptor.
    unsigned char bDescriptorType;       // ENDPOINT descriptor type (USB_DESCRIPTOR_ENDPOINT).
    unsigned char bEndpointAddress;      // Endpoint address. Bit 7 indicates direction (0=OUT, 1=IN).
    unsigned char bmAttributes;          // Endpoint transfer type.
    unsigned short wMaxPacketSize;        // Maximum packet size.
    unsigned char bInterval;             // Polling interval in frames.
} USB_ENDPOINT_DESCRIPTOR;

typedef struct __attribute__ ((__packed__)) _USB_DEVICE_QUALIFIER_DESCRIPTOR
{
    unsigned char bLength;               // Size of this descriptor
    unsigned char bType;                 // Type, always USB_DESCRIPTOR_DEVICE_QUALIFIER
    unsigned short bcdUSB;                // USB spec version, in BCD
    unsigned char bDeviceClass;          // Device class code
    unsigned char bDeviceSubClass;       // Device sub-class code
    unsigned char bDeviceProtocol;       // Device protocol
    unsigned char bMaxPacketSize0;       // EP0, max packet size
    unsigned char bNumConfigurations;    // Number of "other-speed" configurations
    unsigned char bReserved;             // Always zero (0)
} USB_DEVICE_QUALIFIER_DESCRIPTOR;

//Generic Configuration
typedef struct __attribute__ ((__packed__)) _CONFIG_GEN
{
 USB_CONFIGURATION_DESCRIPTOR dev_conf;
 USB_INTERFACE_DESCRIPTOR dev_int;
} CONFIG_GEN;

//HID
typedef struct __attribute__ ((__packed__)) _USB_HID_DESCRIPTOR
{
    unsigned char bLength;
    unsigned char bDescriptorType;
    unsigned short bcdHID;
    unsigned char bCountryCode;
    unsigned char bNumDescriptors;
    unsigned char bRPDescriptorType;
    unsigned short wRPDescriptorLength;
} USB_HID_DESCRIPTOR;

//Configuration
typedef struct __attribute__ ((__packed__)) _CONFIG_HID
{
 USB_CONFIGURATION_DESCRIPTOR dev_conf;
 USB_INTERFACE_DESCRIPTOR dev_int;
 USB_HID_DESCRIPTOR dev_hid;
 USB_ENDPOINT_DESCRIPTOR dev_ep;
} CONFIG_HID;

//CDC
/* Functional Descriptor Structure - See CDC Specification 1.1 for details */

/* Header Functional Descriptor */
typedef struct __attribute__ ((__packed__)) _USB_CDC_HEADER_FN_DSC
{
    unsigned char bFNLength;
    unsigned char bDscType;
    unsigned char bDscSubType;
    unsigned short bcdCDC;
} USB_CDC_HEADER_FN_DSC;

/* Abstract Control Management Functional Descriptor */
typedef struct __attribute__ ((__packed__)) _USB_CDC_ACM_FN_DSC
{
    unsigned char bFNLength;
    unsigned char bDscType;
    unsigned char bDscSubType;
    unsigned char bmCapabilities;
} USB_CDC_ACM_FN_DSC;

/* Union Functional Descriptor */
typedef struct __attribute__ ((__packed__)) _USB_CDC_UNION_FN_DSC
{
    unsigned char bFNLength;
    unsigned char bDscType;
    unsigned char bDscSubType;
    unsigned char bMasterIntf;
    unsigned char bSaveIntf0;
} USB_CDC_UNION_FN_DSC;

/* Call Management Functional Descriptor */
typedef struct __attribute__ ((__packed__)) _USB_CDC_CALL_MGT_FN_DSC
{
    unsigned char bFNLength;
    unsigned char bDscType;
    unsigned char bDscSubType;
    unsigned char bmCapabilities;
    unsigned char bDataInterface;
} USB_CDC_CALL_MGT_FN_DSC;

//Configuration
typedef struct __attribute__ ((__packed__)) _CONFIG_CDC
{
 USB_CONFIGURATION_DESCRIPTOR dev_conf0;
 USB_INTERFACE_DESCRIPTOR dev_int0;
 USB_CDC_HEADER_FN_DSC cdc_header;
 USB_CDC_CALL_MGT_FN_DSC cdc_call_mgt;
 USB_CDC_ACM_FN_DSC cdc_acm;
 USB_CDC_UNION_FN_DSC cdc_union;
 USB_ENDPOINT_DESCRIPTOR dev_ep0;
 USB_INTERFACE_DESCRIPTOR dev_int1;
 USB_ENDPOINT_DESCRIPTOR dev_ep1;
 USB_ENDPOINT_DESCRIPTOR dev_ep2;
} CONFIG_CDC;

//=================================================================================
//USBIP data struct

typedef struct  __attribute__ ((__packed__)) _OP_REQ_DEVLIST
{
 unsigned short version;
 unsigned short command;
 int status;
} OP_REQ_DEVLIST;

typedef struct  __attribute__ ((__packed__)) _OP_REP_DEVLIST_HEADER
{
unsigned short version;
unsigned short command;
int status;
int nExportedDevice;
}OP_REP_DEVLIST_HEADER;

//================= for each device
typedef struct  __attribute__ ((__packed__)) _OP_REP_DEVLIST_DEVICE
{
char usbPath[256];
char busID[32];
int busnum;
int devnum;
int speed;
unsigned short idVendor;
unsigned short idProduct;
unsigned short bcdDevice;
unsigned char bDeviceClass;
unsigned char bDeviceSubClass;
unsigned char bDeviceProtocol;
unsigned char bConfigurationValue;
unsigned char bNumConfigurations;
unsigned char bNumInterfaces;
}OP_REP_DEVLIST_DEVICE;

//================== for each interface
typedef struct  __attribute__ ((__packed__)) _OP_REP_DEVLIST_INTERFACE
{
unsigned char bInterfaceClass;
unsigned char bInterfaceSubClass;
unsigned char bInterfaceProtocol;
unsigned char padding;
}OP_REP_DEVLIST_INTERFACE;

typedef struct  __attribute__ ((__packed__)) _OP_REP_DEVLIST
{
OP_REP_DEVLIST_HEADER      header;
OP_REP_DEVLIST_DEVICE      device; //only one!
OP_REP_DEVLIST_INTERFACE   *interfaces;
}OP_REP_DEVLIST;

typedef struct  __attribute__ ((__packed__)) _OP_REQ_IMPORT
{
unsigned short version;
unsigned short command;
int status;
char busID[32];
}OP_REQ_IMPORT;

typedef struct  __attribute__ ((__packed__)) _OP_REP_IMPORT
{
unsigned short version;
unsigned short command;
int  status;
//------------- if not ok, finish here
char usbPath[256];
char busID[32];
int busnum;
int devnum;
int speed;
unsigned short idVendor;
unsigned short idProduct;
unsigned short bcdDevice;
unsigned char bDeviceClass;
unsigned char bDeviceSubClass;
unsigned char bDeviceProtocol;
unsigned char bConfigurationValue;
unsigned char bNumConfigurations;
unsigned char bNumInterfaces;
}OP_REP_IMPORT;

typedef struct  __attribute__ ((__packed__)) _USBIP_CMD_SUBMIT
{
int command;
int seqnum;
int devid;
int direction;
int ep;
int transfer_flags;
int transfer_buffer_length;
int start_frame;
int number_of_packets;
int interval;
long long setup;
}USBIP_CMD_SUBMIT;

/*
+  Allowed transfer_flags  | value      | control | interrupt | bulk     | isochronous
+ -------------------------+------------+---------+-----------+----------+-------------
+  URB_SHORT_NOT_OK        | 0x00000001 | only in | only in   | only in  | no
+  URB_ISO_ASAP            | 0x00000002 | no      | no        | no       | yes
+  URB_NO_TRANSFER_DMA_MAP | 0x00000004 | yes     | yes       | yes      | yes
+  URB_NO_FSBR             | 0x00000020 | yes     | no        | no       | no
+  URB_ZERO_PACKET         | 0x00000040 | no      | no        | only out | no
+  URB_NO_INTERRUPT        | 0x00000080 | yes     | yes       | yes      | yes
+  URB_FREE_BUFFER         | 0x00000100 | yes     | yes       | yes      | yes
+  URB_DIR_MASK            | 0x00000200 | yes     | yes       | yes      | yes
*/

typedef struct  __attribute__ ((__packed__)) _USBIP_RET_SUBMIT
{
int command;
int seqnum;
int devid;
int direction;
int ep;
int status;
int actual_length;
int start_frame;
int number_of_packets;
int error_count;
long long setup;
}USBIP_RET_SUBMIT;

typedef struct  __attribute__ ((__packed__)) _USBIP_CMD_UNLINK
{
int command;
int seqnum;
int devid;
int direction;
int ep;
int seqnum_urb;
}USBIP_CMD_UNLINK;

typedef struct  __attribute__ ((__packed__)) _USBIP_RET_UNLINK
{
int command;
int seqnum;
int devid;
int direction;
int ep;
int status;
}USBIP_RET_UNLINK;

typedef struct  __attribute__ ((__packed__)) _StandardDeviceRequest
{
  unsigned char bmRequestType;
  unsigned char bRequest;
  unsigned char wValue0;
  unsigned char wValue1;
  unsigned char wIndex0;
  unsigned char wIndex1;
  unsigned short wLength;
}StandardDeviceRequest;

typedef struct {
    int vendorId;
    int productId;
    unsigned char I;    
    unsigned int P;
    unsigned char U;    
} usb_ids;

class USBIPDevice
{
public:
    USBIPDevice();

    void OnStateChanged(std::function<void(void)>);
    void OnMessage(std::function<void(std::string)>);
    void OnAttached(std::function<void(void)>);
    void OnAttachReady(std::function<void(void)>);

    int GetServerPort();
    void SetServerPort(int);

    bool IsRunning();
    void Start();
    void Stop();
    void SetIds(usb_ids);

protected:
    void handle_data(int sockfd, USBIP_RET_SUBMIT *usb_req, int bl);
    void handle_unknown_control(int sockfd, StandardDeviceRequest * control_req, USBIP_RET_SUBMIT *usb_req);
    void send_usb_req(int sockfd, USBIP_RET_SUBMIT * usb_req, char * data, unsigned int size, unsigned int status);
    void usbip_run();
    void usbip_stop();

    unsigned char                       keyboard_report[0x3F];
    USB_DEVICE_DESCRIPTOR               dev_dsc;
    CONFIG_HID                          configuration_hid;
    USB_DEVICE_QUALIFIER_DESCRIPTOR     dev_qua;

    std::function<void(void)> state_changed_callback;
    std::function<void(void)> attached_callback;
    std::function<void(void)> attach_ready_callback;
    std::function<void(std::string)> message_callback;

private:
    void handle_device_list(const USB_DEVICE_DESCRIPTOR *dev_dsc, OP_REP_DEVLIST *list);
    void handle_attach(const USB_DEVICE_DESCRIPTOR *dev_dsc, OP_REP_IMPORT *rep);
    int  handle_get_descriptor(int sockfd, StandardDeviceRequest * control_req, USBIP_RET_SUBMIT *usb_req);
    int  handle_set_configuration(int sockfd, StandardDeviceRequest * control_req, USBIP_RET_SUBMIT *usb_req);
    void handle_usb_control(int sockfd, USBIP_RET_SUBMIT *usb_req);
    void handle_usb_request(int sockfd, USBIP_RET_SUBMIT *ret, int bl);    
    void print_recv(char* buff, int size, const char* desc);

    bool running = false;
    int server_port;

    std::thread* thread = nullptr;
    void ThreadFunction();

    int vendor_id;
    int product_id;
};

#endif // USBIP_DEVICE_H
