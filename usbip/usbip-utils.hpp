#ifndef USBIP_UTILS_HPP
#define USBIP_UTILS_HPP

#ifdef _WIN32
#include <winsock.h>
#else
#include <netinet/in.h>
#endif

class USBIPUtils {
public:
    static void pack(int* data, int size)
    {
        int i;
        size = size / 4;
        for(i = 0; i < size; i++)
        {
            data[i] = htonl(data[i]);
        }
        //swap setup
        i = data[size-1];
        data[size - 1] = data[size - 2];
        data[size - 2] = i;
    }

    static void unpack(int* data, int size)
    {
        int i;
        size = size / 4;
        for(i = 0 ; i < size; i++)
        {
            data[i] = ntohl(data[i]);
        }
        //swap setup
        i = data[size - 1];
        data[size - 1] = data[size - 2];
        data[size - 2] = i;
    }

};

#endif // USBIP_UTILS_HPP
