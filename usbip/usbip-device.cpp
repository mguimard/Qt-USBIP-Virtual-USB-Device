#include "usbip-device.h"
#include "usbip-utils.hpp"

USBIPDevice::USBIPDevice()
{
    server_port = 3240;

    const unsigned char I = 0;
    const unsigned char P = 0x0001;
    const unsigned char U = 0x02;

    /* Device Descriptor */
    dev_dsc =
    {
        0x12,                   // Size of this descriptor in bytes
        0x01,                   // DEVICE descriptor type
        0x0110,                 // USB Spec Release Number in BCD format
        0x00,                   // Class Code
        0x00,                   // Subclass code
        0x00,                   // Protocol code
        0x08,                   // Max packet size for EP0, see usb_config.h
        0x1234,                 // Vendor ID
        0x5678,                 // Product ID: Mouse in a circle fw demo
        0x0000,                 // Device release number in BCD format
        0x00,                   // Manufacturer string index
        0x00,                   // Product string index
        0x00,                   // Device serial number string index
        0x01                    // Number of possible configurations
    };

    /* Configuration 1 Descriptor */
    configuration_hid={
        {
            /* Configuration Descriptor */
            0x09,//sizeof(USB_CFG_DSC),    // Size of this descriptor in bytes
            USB_DESCRIPTOR_CONFIGURATION,                // CONFIGURATION descriptor type
            0x0022,                 // Total length of data for this cfg
            1,                      // Number of interfaces in this cfg
            1,                      // Index value of this configuration
            0,                      // Configuration string index
            0x80,
            50,                     // Max power consumption (2X mA)
        },
        {
            /* Interface Descriptor */
            0x09,//sizeof(USB_INTF_DSC),   // Size of this descriptor in bytes
            USB_DESCRIPTOR_INTERFACE,               // INTERFACE descriptor type
            I,                      // Interface Number
            0,                      // Alternate Setting Number
            1,                      // Number of endpoints in this intf
            0x03,                   // Class code
            0x01,                   // Subclass code
            0x01,                   // Protocol code
            0,                      // Interface string index
        },
        {
            /* HID Class-Specific Descriptor */
            0x09,               // Size of this descriptor in bytes RRoj hack
            0x21,                // HID descriptor type
            0x0001,                 // HID Spec Release Number in BCD format (1.11)
            0x00,                   // Country Code (0x00 for Not supported)
            0x01,         // Number of class descriptors, see usbcfg.h
            0x22,                // Report descriptor type
            DESCRIPTOR_SIZE,           // Size of the report descriptor
        },
        {
            /* Endpoint Descriptor */
            0x07,/*sizeof(USB_EP_DSC)*/
            USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
            0x81,            //EndpointAddress
            0x03,                       //Attributes
            0x0008,                  //size
            0xFF                        //Interval
        }
    };

    //Class specific descriptor - HID keyboard
    unsigned char descriptor [DESCRIPTOR_SIZE] = {
        0x06, 0x00, 0xff,   // Usage Page (Vendor Defined 0xFF00)
        0x09, 0x01          // Usage (0x01)
    };

    memcpy(keyboard_report, descriptor, sizeof(descriptor));
}

void USBIPDevice::SetIds(usb_ids ids)
{
    dev_dsc.idVendor = ids.vendorId;
    dev_dsc.idProduct = ids.productId;
    configuration_hid.dev_int.iInterface = ids.I;
    keyboard_report[1] = ids.P;
    keyboard_report[2] = ids.P >> 8;
    keyboard_report[4] = ids.U;
}

void USBIPDevice::Start()
{
    if(thread != nullptr) return;

    message_callback("Starting server...");
    thread = new std::thread(&USBIPDevice::ThreadFunction, this);
}

void USBIPDevice::Stop()
{
    if(thread == nullptr) return;

    message_callback("Stopping server...");

    usbip_stop();
    thread->join();
    delete thread;
    thread = nullptr;

    state_changed_callback();

    message_callback("Server stopped...");
}


void USBIPDevice::ThreadFunction()
{
    usbip_run();
}


int USBIPDevice::GetServerPort()
{
    return server_port;
}

void USBIPDevice::SetServerPort(int value)
{
    server_port = value;
}

bool USBIPDevice::IsRunning()
{
    return running;
}


void USBIPDevice::print_recv(char* buff, int size, const char* desc)
{
    std::string out(desc);
    out.append(" ");

    for(int i = 0; i < size; i++)
    {
        char converted[3];
        sprintf(&converted[0], "%02X", (unsigned char) buff[i]);
        out.append(std::string(converted, 2));
        out.append(" ");
    }

    message_callback(out);
}

void USBIPDevice::handle_device_list(const USB_DEVICE_DESCRIPTOR *dev_dsc, OP_REP_DEVLIST *list)
{
    const char* configuration = (const char*) &configuration_hid;
    CONFIG_GEN* conf= (CONFIG_GEN*) configuration;

    list->header.version = htons(273);
    list->header.command = htons(5);
    list->header.status = 0;
    list->header.nExportedDevice = htonl(1);

    memset(list->device.usbPath,0,256);
    strcpy(list->device.usbPath, "/sys/devices/pci0000:00/0000:00:01.2/usb1/1-1");

    memset(list->device.busID, 0, 32);
    strcpy(list->device.busID, "1-1");

    list->device.busnum                 = htonl(1);
    list->device.devnum                 = htonl(2);
    list->device.speed                  = htonl(2);
    list->device.idVendor               = htons(dev_dsc->idVendor);
    list->device.idProduct              = htons(dev_dsc->idProduct);
    list->device.bcdDevice              = htons(dev_dsc->bcdDevice);
    list->device.bDeviceClass           = dev_dsc->bDeviceClass;
    list->device.bDeviceSubClass        = dev_dsc->bDeviceSubClass;
    list->device.bDeviceProtocol        = dev_dsc->bDeviceProtocol;
    list->device.bConfigurationValue    = conf->dev_conf.bConfigurationValue;
    list->device.bNumConfigurations     = dev_dsc->bNumConfigurations;
    list->device.bNumInterfaces         = conf->dev_conf.bNumInterfaces;

    list->interfaces = (OP_REP_DEVLIST_INTERFACE *) malloc(list->device.bNumInterfaces * sizeof(OP_REP_DEVLIST_INTERFACE));

    /*USB_INTERFACE_DESCRIPTOR *interfaces[0];

    for(int i = 0; i < list->device.bNumInterfaces; i++)
    {
        list->interfaces[i].bInterfaceClass     = interfaces[i]->bInterfaceClass;
        list->interfaces[i].bInterfaceSubClass  = interfaces[i]->bInterfaceSubClass;
        list->interfaces[i].bInterfaceProtocol  = interfaces[i]->bInterfaceProtocol;
        list->interfaces[i].padding             = 0;
    }*/
}


void USBIPDevice::handle_attach(const USB_DEVICE_DESCRIPTOR *dev_dsc, OP_REP_IMPORT *rep)
{
    const char *configuration = (const char *) &configuration_hid;
    CONFIG_GEN * conf= (CONFIG_GEN *)configuration;

    rep->version                = htons(273);
    rep->command                = htons(3);
    rep->status                 = 0;

    memset(rep->usbPath,0,256);
    strcpy(rep->usbPath,"/sys/devices/pci0000:00/0000:00:01.2/usb1/1-1");
    memset(rep->busID,0,32);
    strcpy(rep->busID,"1-1");

    rep->busnum                 = htonl(1);
    rep->devnum                 = htonl(2);
    rep->speed                  = htonl(2);
    rep->idVendor               = dev_dsc->idVendor;
    rep->idProduct              = dev_dsc->idProduct;
    rep->bcdDevice              = dev_dsc->bcdDevice;
    rep->bDeviceClass           = dev_dsc->bDeviceClass;
    rep->bDeviceSubClass        = dev_dsc->bDeviceSubClass;
    rep->bDeviceProtocol        = dev_dsc->bDeviceProtocol;
    rep->bNumConfigurations     = dev_dsc->bNumConfigurations;
    rep->bConfigurationValue    = conf->dev_conf.bConfigurationValue;
    rep->bNumInterfaces         = conf->dev_conf.bNumInterfaces;
}

int USBIPDevice::handle_get_descriptor(int sockfd, StandardDeviceRequest * control_req, USBIP_RET_SUBMIT *usb_req)
{
    const char *configuration = (const char *) &configuration_hid;

    int handled = 0;

    printf("handle_get_descriptor %u [%u]\n",control_req->wValue1,control_req->wValue0 );

    if(control_req->wValue1 == 0x1) // Device
    {
        printf("Device\n");
        handled = 1;
        send_usb_req(sockfd,usb_req, (char *)&dev_dsc, sizeof(USB_DEVICE_DESCRIPTOR)/*control_req->wLength*/, 0);
    }

    if(control_req->wValue1 == 0x2) // configuration
    {
        printf("Configuration\n");
        handled = 1;
        send_usb_req(sockfd,usb_req, (char *) configuration, control_req->wLength ,0);
    }

    unsigned char *strings[0];

    if(control_req->wValue1 == 0x3) // string
    {
        char str[255];

        memset(str,0,255);

        for(int i = 0; i < (*strings[control_req->wValue0] / 2) -1; i++)
        {
            str[i] = strings[control_req->wValue0][i * 2 + 2];
        }

        printf("String (%s)\n", str);

        handled = 1;

        send_usb_req(sockfd,usb_req, (char *) strings[control_req->wValue0] ,*strings[control_req->wValue0]  ,0);
    }

    if(control_req->wValue1 == 0x6) // qualifier
    {
        printf("Qualifier\n");

        handled = 1;

        send_usb_req(sockfd,usb_req, (char *) &dev_qua, control_req->wLength ,0);
    }

    if(control_req->wValue1 == 0xA) // config status ???
    {
        printf("Unknow\n");

        handled = 1;

        char data[0];
        send_usb_req(sockfd,usb_req, data, 0, 1);
    }
    return handled;
}

int USBIPDevice::handle_set_configuration(int sockfd, StandardDeviceRequest * control_req, USBIP_RET_SUBMIT *usb_req)
{
    int handled = 0;

    printf("handle_set_configuration %u[%u]\n",control_req->wValue1,control_req->wValue0 );

    handled = 1;
    char data[0];
    send_usb_req(sockfd, usb_req, data, 0, 0);

    return handled;
}

void USBIPDevice::handle_usb_control(int sockfd, USBIP_RET_SUBMIT *usb_req)
{
    printf("handle_usb_control\n");

    int handled = 0;
    StandardDeviceRequest control_req;

#ifdef _WIN32
    printf("%016I64X\n",usb_req->setup);
#else
    printf("%016llX\n",usb_req->setup);
#endif

    control_req.bmRequestType   =  (usb_req->setup & 0xFF00000000000000)>>56;
    control_req.bRequest        =  (usb_req->setup & 0x00FF000000000000)>>48;
    control_req.wValue0         =  (usb_req->setup & 0x0000FF0000000000)>>40;
    control_req.wValue1         =  (usb_req->setup & 0x000000FF00000000)>>32;
    control_req.wIndex0         =  (usb_req->setup & 0x00000000FF000000)>>24;
    control_req.wIndex1         =  (usb_req->setup & 0x0000000000FF0000)>>16;
    control_req.wLength         =  ntohs(usb_req->setup & 0x000000000000FFFF);

    printf("  UC Request Type %u\n",control_req.bmRequestType);
    printf("  UC Request %u\n",control_req.bRequest);
    printf("  UC Value  %u[%u]\n",control_req.wValue1,control_req.wValue0);
    printf("  UCIndex  %u-%u\n",control_req.wIndex1,control_req.wIndex0);
    printf("  UC Length %u\n",control_req.wLength);

    if(control_req.bmRequestType == 0x80) // Host Request
    {
        if(control_req.bRequest == 0x06) // Get Descriptor
        {
            handled = handle_get_descriptor(sockfd, &control_req, usb_req);
        }

        if(control_req.bRequest == 0x00) // Get STATUS
        {
            char data[2];

            data[0] = 0x01;
            data[1] = 0x00;

            send_usb_req(sockfd,usb_req, data, 2 , 0);

            handled = 1;

            printf("GET_STATUS\n");
        }
    }

    if(control_req.bmRequestType == 0x00) //
    {
        if(control_req.bRequest == 0x09) // Set Configuration
        {
            handled = handle_set_configuration(sockfd, &control_req, usb_req);
        }
    }

    if(control_req.bmRequestType == 0x01)
    {
        if(control_req.bRequest == 0x0B) //SET_INTERFACE
        {
            printf("SET_INTERFACE\n");
            char data[0];
            send_usb_req(sockfd,usb_req, data, 0, 1);

            handled = 1;
        }
    }

    if(!handled)
    {
        printf("Not handled\n");
        handle_unknown_control(sockfd, &control_req, usb_req);
        printf("Not handled WTF\n");
    }

    printf("handle_usb_control done\n");
}

void USBIPDevice::handle_usb_request(int sockfd, USBIP_RET_SUBMIT *ret, int bl)
{
    if(ret->ep == 0)
    {
        printf("#control requests\n");
        handle_usb_control(sockfd, ret);
    }
    else
    {
        printf("#data requests\n");
        handle_data(sockfd, ret, bl);
    }
}

void USBIPDevice::handle_data(int /*sockfd*/, USBIP_RET_SUBMIT* /*usb_req*/, int /*bl*/)
{
    // Sending random keyboard data
    // Send data only for 5 seconds
    /*static int  count=0;
    char return_val[8];
    printf("data\n");
    memset(return_val,0,8);

    if (count < 20)
    {
        if((count % 2 ) == 0)
            return_val[2]=(char)((((25l*rand())/RAND_MAX))+4);

        send_usb_req(sockfd, usb_req, return_val, 8, 0);
    }

    usleep(250000);
    count=count+1;*/
}

void USBIPDevice::handle_unknown_control(int sockfd, StandardDeviceRequest * control_req, USBIP_RET_SUBMIT *usb_req)
{
    printf("handle_unknown_control %d\n", control_req->bmRequestType);

    if(control_req->bmRequestType == 0x81)
    {
        if(control_req->bRequest == 0x6)  //# Get Descriptor
        {
            if(control_req->wValue1 == 0x22)  // send initial report
            {
                printf("send initial report\n");
                send_usb_req(sockfd, usb_req, (char *) keyboard_report, DESCRIPTOR_SIZE, 0);
            }
        }
    }

    if(control_req->bmRequestType == 0x21)  // Host Request
    {
        if(control_req->bRequest == 0x0a)  // set idle
        {
            printf("Idle\n");
            // Idle
            char d[0];
            send_usb_req(sockfd, usb_req, d, 0, 0);
        }

        if(control_req->bRequest == 0x09)  // set report
        {
            printf("set report\n");

            char data[2048];

            if ((recv (sockfd, data, control_req->wLength, 0)) != control_req->wLength)
            {

                printf ("receive error : %s \n", strerror (errno));
                return;
                //exit(-1);
            };

            printf("set report: report size %d\n", control_req->wLength);

            print_recv(data, control_req->wLength, "SET_REPORT");

            printf("set report: no error\n");

            char d[0];

            send_usb_req(sockfd, usb_req, d, 0, 0);
        }
    }

    printf("handle_unknown_control done\n");
}

void USBIPDevice::send_usb_req(int sockfd, USBIP_RET_SUBMIT * usb_req, char * data, unsigned int size, unsigned int status)
{
    usb_req->command            = 0x3;
    usb_req->status             = status;
    usb_req->actual_length      = size;
    usb_req->start_frame        = 0x0;
    usb_req->number_of_packets  = 0x0;

    usb_req->setup              = 0x0;
    usb_req->devid              = 0x0;
    usb_req->direction          = 0x0;
    usb_req->ep                 = 0x0;

    USBIPUtils::pack((int *)usb_req, sizeof(USBIP_RET_SUBMIT));

    printf("send_usb_req\n");

    if (send (sockfd, (char *)usb_req, sizeof(USBIP_RET_SUBMIT), 0) != sizeof(USBIP_RET_SUBMIT))
    {
        printf ("send error : %s \n", strerror (errno));
        return;//exit(-1);
    }

    printf("send_usb_req size: %d\n", size);
    if(size > 0)
    {
        if (send (sockfd, data, size, 0) != size)
        {
            printf ("send error : %s \n", strerror (errno));
            return;//exit(-1);
        }
    }
    printf("send_usb_req done\n");
}

/* simple TCP server */
void USBIPDevice::usbip_run()
{
    running = true;
    state_changed_callback();

    struct sockaddr_in serv, cli;
    int listenfd, sockfd, nb;

    struct timeval timeout;
    timeout.tv_sec  = 5;
    timeout.tv_usec = 0;

#ifdef _WIN32
    int clilen;
#else
    unsigned int clilen;

#endif
    unsigned char attached;

#ifdef _WIN32
    WSAStartup (wVersionRequested, &wsaData);
    if (wsaData.wVersion != wVersionRequested)
    {
        fprintf (stderr, "\n Wrong version\n");
        exit (-1);
    }
#endif

    if ((listenfd = socket (PF_INET, SOCK_STREAM, 0)) < 0)
    {
        std::string str = "Socket error " ;
        str.append(strerror (errno));
        message_callback(str);
        // todo => dont crash
        return;//exit (1);
    }

    int reuse = 1;


    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0)
    {
        perror("setsockopt(SO_REUSEADDR) failed");
    }

    memset(&serv, 0, sizeof(serv));

    serv.sin_family         = AF_INET;
    serv.sin_addr.s_addr    = htonl (INADDR_ANY);
    serv.sin_port           = htons (server_port);

    if (bind(listenfd, (sockaddr *) & serv, sizeof(serv)) < 0)
    {
        printf ("bind error : %s \n", strerror (errno));
        // todo => dont crash
        return;//exit (1);
    }

    if (listen (listenfd, SOMAXCONN) < 0)
    {
        printf ("listen error : %s \n", strerror (errno));
        // todo => dont crash
        return;//exit (1);
    }

    message_callback("Server listening");

    attach_ready_callback();

    for (;;)
    {
        if(!running)
        {
            break;
        }

        clilen = sizeof (cli);

        if ((sockfd = accept (listenfd, (sockaddr *) & cli,  & clilen)) < 0)
        {
            printf ("accept error : %s \n", strerror (errno));
            // todo => dont crash
            return;// exit (1);
        };

        setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

        printf("Connection address:%s\n",inet_ntoa(cli.sin_addr));

        attached=0;

        while(running)
        {
            if(!attached)
            {
                OP_REQ_DEVLIST req;

                nb = recv (sockfd, (char *)&req, sizeof(OP_REQ_DEVLIST), 0);

                if (nb != sizeof(OP_REQ_DEVLIST))
                {
                    //printf ("receive error : %s \n", strerror (errno));
                    break;
                };
#ifdef _DEBUG
                print_recv((char *)&req, sizeof(OP_REQ_DEVLIST),"OP_REQ_DEVLIST");
#endif
                req.command = ntohs(req.command);

                printf("Header Packet\n");
                printf("command: 0x%02X\n",req.command);

                if(req.command == 0x8005)
                {
                    OP_REP_DEVLIST list;
                    printf("list of devices\n");

                    handle_device_list(&dev_dsc, &list);

                    if (send (sockfd, (char *)&list.header, sizeof(OP_REP_DEVLIST_HEADER), 0) != sizeof(OP_REP_DEVLIST_HEADER))
                    {
                        printf("send error : %s \n", strerror (errno));
                        break;
                    }

                    if (send (sockfd, (char *)&list.device, sizeof(OP_REP_DEVLIST_DEVICE), 0) != sizeof(OP_REP_DEVLIST_DEVICE))
                    {
                        printf("send error : %s \n", strerror (errno));
                        break;
                    }

                    if (send (sockfd, (char *)list.interfaces, sizeof(OP_REP_DEVLIST_INTERFACE)*list.device.bNumInterfaces, 0) != sizeof(OP_REP_DEVLIST_INTERFACE)/**list.device.bNumInterfaces*/)
                    {
                        printf("send error : %s \n", strerror (errno));
                        break;
                    }

                    free(list.interfaces);
                }
                else if(req.command == 0x8003)
                {
                    char busid[32];

                    OP_REP_IMPORT rep;

                    printf("attach device\n");

                    nb = recv (sockfd, busid, 32, 0);

                    if (nb == -1)
                    {
                        if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
                        {
                            printf ("receive error : %s \n", strerror (errno));
                            break;
                        }
                    }

                    //print_recv(busid, 32,"Busid");

                    handle_attach(&dev_dsc, &rep);

                    if (send (sockfd, (char *)&rep, sizeof(OP_REP_IMPORT), 0) != sizeof(OP_REP_IMPORT))
                    {
                        printf ("send error : %s \n", strerror (errno));
                        break;
                    }

                    attached = 1;

                    message_callback("Device attached");

                    attached_callback();
                }
            }
            else
            {
                printf("------------------------------------------------\n");
                printf("handles requests\n");

                USBIP_CMD_SUBMIT cmd;
                USBIP_RET_SUBMIT usb_req;

                nb = recv (sockfd, (char *)&cmd, sizeof(USBIP_CMD_SUBMIT), 0);

                printf("nb received : %d\n", nb);

                if (nb == -1)
                {
                    if(errno == EWOULDBLOCK)
                    {
                        continue;
                    }

                    printf ("receive error : %s \n", strerror (errno));
                    break;
                }


                //print_recv((char *)&cmd, sizeof(USBIP_CMD_SUBMIT),"USBIP_CMD_SUBMIT");

                USBIPUtils::unpack((int *)&cmd,sizeof(USBIP_CMD_SUBMIT));

                printf("usbip cmd %u\n",cmd.command);
                printf("usbip seqnum %u\n",cmd.seqnum);
                printf("usbip devid %u\n",cmd.devid);
                printf("usbip direction %u\n",cmd.direction);
                printf("usbip ep %u\n",cmd.ep);
                printf("usbip flags %u\n",cmd.transfer_flags);
                printf("usbip number of packets %u\n",cmd.number_of_packets);
                printf("usbip interval %u\n",cmd.interval);
#ifdef _WIN32
                printf("usbip setup %I64u\n",cmd.setup);
#else
                printf("usbip setup %llu\n",cmd.setup);
#endif
                printf("usbip buffer lenght  %u\n",cmd.transfer_buffer_length);

                usb_req.command             = 0;
                usb_req.seqnum              = cmd.seqnum;
                usb_req.devid               = cmd.devid;
                usb_req.direction           = cmd.direction;
                usb_req.ep                  = cmd.ep;
                usb_req.status              = 0;
                usb_req.actual_length       = 0;
                usb_req.start_frame         = 0;
                usb_req.number_of_packets   = 0;
                usb_req.error_count         = 0;
                usb_req.setup               = cmd.setup;

                if(cmd.command == 1)
                {
                    handle_usb_request(sockfd, &usb_req, cmd.transfer_buffer_length);
                }

                if(cmd.command == 2) //unlink urb
                {
                    printf("####################### Unlink URB %u  (not working!!!)\n",cmd.transfer_flags);
                }

                if(cmd.command > 2)
                {
                    printf("Unknown USBIP cmd!\n");
                    //close (sockfd);
                    //close (listenfd);
#ifdef _WIN32
                    WSACleanup ();
#endif
                    //return;
                }

            }
        }
        close (sockfd);
        close (listenfd);
    }
#ifdef _WIN32
    WSACleanup ();
#endif

    state_changed_callback();
}

void USBIPDevice::usbip_stop()
{
    running = false;
}

void USBIPDevice::OnAttached(std::function<void(void)> cb)
{
    attached_callback = cb;
}

void USBIPDevice::OnAttachReady(std::function<void(void)> cb)
{
    attach_ready_callback = cb;
}

void USBIPDevice::OnStateChanged(std::function<void(void)> cb)
{
    state_changed_callback = cb;
}

void USBIPDevice::OnMessage(std::function<void(std::string)> cb)
{
    message_callback = cb;
}
