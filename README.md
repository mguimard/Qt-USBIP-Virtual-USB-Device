# Qt-USBIP-Virtual-USB-Device

Qt app that ease virtual USB devices creation.

Code is ported from https://github.com/lcgamboa/USBIP-Virtual-USB-Device 

## Requirements

vhci kernel module

`sudo modprobe vhci-hcd`

usbip (example for debian)

`sudo apt install usbip`

## Installation

Clone this repository, compile with qt and run it.

Add a new device, and run `sudo usbip attach -r 127.0.0.1 -b 1-1` from a terminal

Run `lsusb` to check the device is attached.

