QT       += core gui widgets
CONFIG += c++17
QMAKE_CXXFLAGS += -std=c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    usbip/usbip-device.cpp \
    qt/mainwindow.cpp \
    qt/virtualhiddeviceview.cpp \
    qt/virtualhiddeviceslist.cpp \

HEADERS += \
    usbip/usbip-device.h \
    usbip/usbip-utils.hpp \
    qt/mainwindow.h \
    qt/virtualhiddeviceview.h \
    qt/virtualhiddeviceslist.h \

INCLUDEPATH += \
    usbip \
    qt

FORMS += \
    qt/mainwindow.ui \
    qt/virtualhiddeviceslist.ui \
    qt/virtualhiddeviceview.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
